" -------------------------------------------------------------------------- "
"                                                                            "
"   (   (  (     )                                                           "
"   )\  )\ )\   (                Andrew Rowles                               "
"  ((_)((_|(_)  )\  '            02/27/2016                                  "
"  \ \ / / (_)_((_))                                                         "
"   \ V /  | | '  \()                                                        "
"    \_/   |_|_|_|_|                                                         "
"                                                                            "
" -------------------------------------------------------------------------- "

" -------------------------------------------------------------------------- "
" General Vundle Settings
" -------------------------------------------------------------------------- "

" Set Vim Improved
set nocompatible
filetype off

" Set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

let g:ycm_global_ycm_extra_conf = '~/.ycm_extra_conf.py'


Plugin 'tpope/vim-fugitive'
Plugin 'lervag/vimtex'
Plugin 'Valloric/YouCompleteMe'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'LaTeX-Box-Team/LaTeX-Box'
Plugin 'sjl/badwolf'

" Plugins must be called before these required lines
call vundle#end()
filetype plugin indent on


" -------------------------------------------------------------------------- "
" General Settings
" -------------------------------------------------------------------------- "

" Filetype plugins
filetype plugin on
filetype indent on


" -------------------------------------------------------------------------- "
" Editor Settings
" -------------------------------------------------------------------------- "

syntax enable

" Enable UTF-8
scriptencoding utf-8
set encoding=utf-8

" Set auto update when file is changed from outside
set autoread

" Indentation
set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

" Linebreak on 500 characters
set lbr
set tw=500

" Wrap lines at convenient points
" set linebreak 

" Enable auto indent
set ai

" Enable smart indent
set si

" Enable wrap lines
set wrap


" -------------------------------------------------------------------------- "
" Interface Settings
" -------------------------------------------------------------------------- "

" Show Vim mode
set showmode

" Display newline character
set list
set listchars=tab:▸\ ,eol:¬

" Show line numbers
set number

" Set height of the command bar
set cmdheight=2

" Highlight search results
set hlsearch

" Make search act like search in modern browsers
set incsearch

" Add a bit extra margin to the left
set foldcolumn=1

" Set the background & theme
set background=dark
colorscheme badwolf

" Show the current position
set ruler


if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

"  => Status line
" " Always show the status line
set laststatus=2
" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l



" -------------------------------------------------------------------------- "
" Navigation Settings
" -------------------------------------------------------------------------- "

" Set number of lines for cursor to jump when using j/k
set so=7

" Enable mouse control
if has('mouse')
  set mouse=a
endif


" -------------------------------------------------------------------------- "
" Sound Settings
" -------------------------------------------------------------------------- "

" Disable sounds
set noerrorbells
set novisualbell
set t_vb=
set tm=500



" 
function! HasPaste()
      if &paste
                return 'PASTE MODE  '
                    endif
                        return ''
                      endfunction
